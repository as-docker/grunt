CMD=$(filter-out $@,$(MAKECMDGOALS))

getIP: ; @docker inspect --format '{{ .NetworkSettings.IPAddress }}' ${CMD}

grunt: ; @docker run --rm -it \
    -v ~/.ssh/:/home/nodejs/.ssh \
    -v $$(pwd)/:/opt \
    amsdard/grunt grunt ${CMD}

%: ; @:
